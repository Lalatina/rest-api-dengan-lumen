<?php

namespace App\Http\Controllers;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showall()
    {
        $data_user= User::paginate(1);
        
        return response()->json([
            'success' => true,
            'massege' => 'success',
            'data' => $data_user
        ],201);
    }
}
