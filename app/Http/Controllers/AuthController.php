<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        $data_user = User::where('email', $request->input('email'))->first();
        
        if (Hash::check($request->input('password'), $data_user->password)) {
            $gen_token = base64_encode(str_random(40));

            $data_user->update([
                'api_token'=>$gen_token
            ]);
            return response()->json([
                'success' => true,
                'massege' => 'Login Berhasil',
                'data' => [
                    'user' => $data_user,
                    'api_token'=>$gen_token
                ]
            ],201);

        }else{
            return response()->json([
                'success' => false,
                'massege' => 'Gagal Login',
                'data' =>  ''
            ],400);
        }
    }
    
    public function register(Request $request)
    {
        $data_push = array(
            'name' => $request->input('name'), 
            'email' => $request->input('email'), 
            'password' => Hash::make($request->input('password')), 
        );
        
        $call = User::create($data_push);
        
        if ($call) {

            return response()->json([
                'success' => true,
                'massege' => 'Berhasil Daftar',
                'data' => $call
            ],201);

        } else {

            return response()->json([
                'success' => false,
                'massege' => 'Gagal Daftar',
                'data' =>  ''
            ],400);

        }
        
    }

    //
}
